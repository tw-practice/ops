package com.thoughtworks.practice.reason;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ItemNotFoundExceptionTest {
    private ItemNotFoundException exception;

    @BeforeEach
    void setup() {
        exception = new ItemNotFoundException();
    }

    @Test
    void shouldGiveExceptionMessageWhenItemIsNotPresentInStock() {
        String expected = "We do not sell the requested item.";
        assertEquals(expected, exception.getItemNotFoundExceptionMessage());
    }
}