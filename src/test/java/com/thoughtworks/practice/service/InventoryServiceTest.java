package com.thoughtworks.practice.service;

import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.stock.Inventory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class InventoryServiceTest {
    private InventoryService inventoryService;
    private Inventory inventory;

    @BeforeEach
    void setup() {
        inventoryService = new InventoryService();
        inventory = new Inventory();
        inventory.load("./src/main/java/com/thoughtworks/practice/stock/resource/Stock.txt");
    }

    @Test
    void shouldBeTrueWhenItemIsPresentInInventory() {
        Item item = new Book("BOOK", 1, 2);
        assertTrue(inventoryService.isItemPresent(item, inventory));
    }

    @Test
    void shouldBeFalseWhenItemIsNotPresentInInventory() {
        Item item = new Book("BOOK", 1000, 2);
        assertFalse(inventoryService.isItemPresent(item, inventory));
    }

    @Test
    void shouldUpdateRemainingQuantityOfAnOrderedItem() throws ItemOutOfStockException {
        Item item = new Book("BOOK", 1, 3);
        inventoryService.updateRemainingQuantity(item, inventory);
        assertEquals(2, inventory.getItemFromStock(item).getQuantity());
    }

    @Test
    void shouldBeTrueWhenItemIsPresentInSufficientQuantity() {
        Item item = new Book("BOOK", 1, 3);
        assertTrue(inventoryService.isItemPresentInSufficientQuantity(item, inventory));
    }

    @Test
    void shouldBeFalseWhenItemIsNotPresentInSufficientQuantity() {
        Item item = new Book("BOOK", 1, 8);
        assertFalse(inventoryService.isItemPresentInSufficientQuantity(item, inventory));
    }

    @Test
    void shouldGiveAllTheMembershipRequestsAvailableInStock() {
        inventoryService = mock(InventoryService.class);
        inventoryService.getMembershipRequestsAvailableInStock(inventory);
        verify(inventoryService).getMembershipRequestsAvailableInStock(inventory);
    }
}