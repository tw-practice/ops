package com.thoughtworks.practice.service;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.item.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OrderServiceTest {
    private OrderService orderService;

    @BeforeEach
    void setup() {
        orderService = new OrderService();
    }

    @Test
    void shouldGiveItemsOfTypeMembership() {
        Item item1 = new Membership("MEMBERSHIP", 101);
        Item item2 = new Membership("MEMBERSHIP", 102);
        List<Item> orderedItems = Arrays.asList(item1, item2);
        Order order = new Order(orderedItems);
        assertEquals(2, orderService.getMembershipsPerOrder(order).size());
    }

    @Test
    void shouldGiveTotalCostPerOrder() {
        Item item1 = new Book("BOOK", 1, 3, 100);
        Item item2 = new Membership("MEMBERSHIP", 102, 200);
        Item item3 = new Video("VIDEO", 1, 50);
        List<Item> orderedItems = Arrays.asList(item1, item2, item3);
        Order order = new Order(orderedItems);
        assertEquals(550, orderService.getTotalCostPerOrder(order));
    }

    @Test
    void shouldGiveTotalNumberOfCopiesOfOrder() {
        Item item1 = new Book("BOOK", 1, 3, 100);
        Item item2 = new Membership("MEMBERSHIP", 102, 200);
        Item item3 = new Video("VIDEO", 1, 50);
        List<Item> orderedItems = Arrays.asList(item1, item2, item3);
        Order order = new Order(orderedItems);
        assertEquals(5, orderService.getTotalNumberOfCopiesPerOrder(order));
    }

    @Test
    void shouldGiveItemsOfTypeUpgradeMembership() {
        Item item1 = new UpgradeMembership("UPGRADE_MEMBERSHIP", 101);
        Item item2 = new UpgradeMembership("UPGRADE_MEMBERSHIP", 102);
        List<Item> orderedItems = Arrays.asList(item1, item2);
        Order order = new Order(orderedItems);
        assertEquals(2, orderService.getUpgradeMembershipsPerOrder(order).size());
    }
}