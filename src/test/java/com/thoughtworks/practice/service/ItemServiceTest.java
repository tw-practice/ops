package com.thoughtworks.practice.service;

import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Membership;
import com.thoughtworks.practice.item.Video;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ItemServiceTest {
    private ItemService itemService;

    @BeforeEach
    void setup() {
        itemService = new ItemService();
    }

    @Test
    void shouldGiveCostOfOneBook() {
        Item book = new Book("BOOK", 1, 4, 100);
        assertEquals(100, itemService.getCostOfOneItem(book));
    }

    @Test
    void shouldGiveCostOfOneVideo() {
        Item video = new Video("VIDEO", 1, 50);
        assertEquals(50, itemService.getCostOfOneItem(video));
    }

    @Test
    void shouldGiveCostOfOneMembershipRequest() {
        Item membership = new Membership("MEMBERSHIP", 101, 200);
        assertEquals(200, itemService.getCostOfOneItem(membership));
    }

    @Test
    void shouldGiveTotalCostPerItem() {
        Item item = new Book("BOOK", 2, 4, 100);
        assertEquals(400, itemService.getCostPerItem(item));
    }
}