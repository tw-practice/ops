package com.thoughtworks.practice;

import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OrderTest {
    private Order order;

    @Test
    void shouldGiveTheOrderedBook() {
        List<Item> orderedItems = Collections.singletonList(new Book("BOOK", 1, 2));
        order = new Order(orderedItems);
        assertEquals(orderedItems, order.getOrderedItems());
    }

    @Test
    void shouldGiveTheOrderedVideo() {
        List<Item> orderedItems = Collections.singletonList(new Video("VIDEO", 1));
        order = new Order(orderedItems);
        assertEquals(orderedItems, order.getOrderedItems());
    }

    @Test
    void shouldGiveTheOrderedItems() {
        List<Item> orderedItems = Arrays.asList(new Book("BOOK", 1, 2), new Video("VIDEO", 2));
        order = new Order(orderedItems);
        assertEquals(orderedItems, order.getOrderedItems());
    }
}
