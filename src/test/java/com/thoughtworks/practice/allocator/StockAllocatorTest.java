package com.thoughtworks.practice.allocator;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.service.ItemOutOfStockException;
import com.thoughtworks.practice.stock.Inventory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StockAllocatorTest {
    private StockAllocator stockAllocator;
    private Inventory inventory;

    @BeforeEach
    void setup() {
        stockAllocator = new StockAllocator();
        inventory = new Inventory();
        inventory.load("./src/main/java/com/thoughtworks/practice/stock/resource/Stock.txt");
    }

    @Test
    void shouldThrowExceptionWhenItemGoesOutOfStock() {
        Item item1 = new Book("BOOK", 1, 2);
        Item item2 = new Book("BOOK", 1, 4);
        Order order = new Order(Arrays.asList(item1, item2));
        assertThrows(ItemOutOfStockException.class, () -> stockAllocator.allocate(order, inventory));
    }

    @Test
    void shouldNotThrowExceptionWhenItemDoesNotGoOutOfStock() {
        Item item1 = new Book("BOOK", 1, 1);
        Item item2 = new Book("BOOK", 1, 4);
        Order order = new Order(Arrays.asList(item1, item2));
        assertDoesNotThrow(() -> stockAllocator.allocate(order, inventory));
    }
}
