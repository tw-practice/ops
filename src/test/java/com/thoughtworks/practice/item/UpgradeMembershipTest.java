package com.thoughtworks.practice.item;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UpgradeMembershipTest {

    @Test
    void shouldGiveUpgradedExpiryDate() {
        UpgradeMembership upgradeMembership = new UpgradeMembership("UPGRADE_MEMBERSHIP", 101, 50);
        assertEquals(LocalDate.now().plusMonths(2), upgradeMembership.getExpiry());
    }
}