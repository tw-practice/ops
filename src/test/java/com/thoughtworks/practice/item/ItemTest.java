package com.thoughtworks.practice.item;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ItemTest {

    @Test
    void shouldGiveItemName() {
        Item item = new Item("BOOK", 1, 2);
        assertEquals("BOOK", item.getName());
    }

    @Test
    void shouldGiveItemID() {
        Item item = new Item("BOOK", 1, 2);
        assertEquals(1, item.getID());
    }

    @Test
    void shouldGiveQuantityOfItem() {
        Item item = new Item("BOOK", 1, 2);
        assertEquals(2, item.getQuantity());
    }

    @Test
    void shouldSetQuantityOfItem() {
        Item item = new Item("BOOK", 1, 2);
        item.setQuantity(10);
        assertEquals(10, item.getQuantity());
    }

    @Test
    void shouldGiveCostOfItem() {
        Item item = new Item("BOOK", 1, 2, 100);
        assertEquals(100, item.getCost());
    }
}