package com.thoughtworks.practice.ui;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DetailInputUITest {
    private DetailInputUI detailInputUI;

    @BeforeEach
    void setup() {
        detailInputUI = new DetailInputUI();
    }

    @Test
    void shouldGiveOrderStartMessage() {
        String expected = "ORDER START \n(ITEM ID QUANTITY)";
        assertEquals(expected, detailInputUI.getOrderStartMessage());
    }

    @Test
    void shouldGiveOrderEndMessage() {
        String expected = "ORDER END";
        assertEquals(expected, detailInputUI.getOrderEndMessage());
    }
}