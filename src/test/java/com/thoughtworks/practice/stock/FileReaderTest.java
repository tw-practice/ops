package com.thoughtworks.practice.stock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class FileReaderTest {
    private FileReader fileReader;

    @BeforeEach
    void setup() {
        fileReader = new FileReader();
    }

    @Test
    void shouldBeNullWhenFileDoesNotExist() {
        String filePath = "";
        assertNull(fileReader.getScannedFile(filePath));
    }

    @Test
    void shouldGetTheFileContent() {
        fileReader = mock(FileReader.class);
        String filepath = "./src/main/java/com/thoughtworks/practice/stock/resource/Stock.txt";
        fileReader.getFileContent(filepath);
        verify(fileReader).getFileContent(filepath);
    }
}