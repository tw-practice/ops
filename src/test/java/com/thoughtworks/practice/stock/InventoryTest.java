package com.thoughtworks.practice.stock;

import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class InventoryTest {
    private Inventory inventory;

    @BeforeEach
    void setup() {
        inventory = new Inventory();
        inventory.load("./src/main/java/com/thoughtworks/practice/stock/resource/Stock.txt");
    }

    @Test
    void shouldGiveTheAvailableStock() {
        inventory = mock(Inventory.class);
        inventory.load("./src/main/java/com/thoughtworks/practice/stock/resource/Stock.txt");
        inventory.getAvailableStock();
        verify(inventory).getAvailableStock();
    }

    @Test
    void shouldGiveIndexOfAnItemPresentInStock() {
        Item item = new Video("VIDEO", 2);
        assertEquals(3, inventory.getItemIndex(item));
    }

    @Test
    void shouldGiveIndexMinus1WhenItemIsNotPresentInStock() {
        assertEquals(-1, inventory.getItemIndex(null));
    }

    @Test
    void shouldGetASpecificItemFromStock() {
        Item item = new Video("VIDEO", 2);
        assertEquals(item, inventory.getItemFromStock(item));
    }

    @Test
    void shouldGiveIndexOfUpdatedItemInStock() {
        Item item = new Book("BOOK", 1, 2);
        int itemIndex = inventory.getItemIndex(item);
        Item updatedItem = new Book("BOOK", 1, 5);
        inventory.updateExistingItemInStock(updatedItem, itemIndex);
        assertEquals(itemIndex, inventory.getItemIndex(updatedItem));
    }
}