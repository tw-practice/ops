package com.thoughtworks.practice.parser.detail;

import com.thoughtworks.practice.item.*;
import com.thoughtworks.practice.parser.StringParser;
import com.thoughtworks.practice.parser.details.DetailStringParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DetailStringParserTest {
    private StringParser stringParser;

    @BeforeEach
    void setup() {
        stringParser = new DetailStringParser();
    }

    @Test
    void shouldGiveStringBookWhenGivenItemBook() {
        Item book = new Book("BOOK", 1, 2);
        assertEquals("BOOK 1 2", stringParser.getStringFromItem(book));
    }

    @Test
    void shouldGiveStringVideoWhenGivenItemVideo() {
        Item video = new Video("VIDEO", 1);
        assertEquals("VIDEO 1", stringParser.getStringFromItem(video));
    }

    @Test
    void shouldGiveParsedStringList() {
        List<Item> stockList = Arrays.asList(new Book("BOOK", 1, 2), new Video("VIDEO", 1));
        List<String> expected = Arrays.asList("BOOK 1 2", "VIDEO 1");
        assertEquals(expected, stringParser.parseStringList(stockList));
    }
}