package com.thoughtworks.practice.parser.detail;

import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import com.thoughtworks.practice.parser.ItemParser;
import com.thoughtworks.practice.parser.details.DetailItemParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DetailItemParserTest {
    private ItemParser itemParser;

    @BeforeEach
    void setup() {
        itemParser = new DetailItemParser();
    }

    @Test
    void shouldParseItemBook() {
        String[] stockDetails = {"BOOK", "1", "2"};
        assertEquals(new Book("BOOK", 1, 2), (itemParser.getItemFromString(stockDetails)));
    }

    @Test
    void shouldParseItemVideo() {
        String[] stockDetails = {"VIDEO", "1"};
        assertEquals(new Video("VIDEO", 1), (itemParser.getItemFromString(stockDetails)));
    }

    @Test
    void shouldGiveItemsInStock() {
        List<String> stockList = Arrays.asList("BOOK 1 2", "VIDEO 1");
        List<Item> expected = Arrays.asList(new Book("BOOK", 1, 2), new Video("VIDEO", 1));
        assertEquals(expected, itemParser.parseItemList(stockList));
    }

    @Test
    void shouldParseItemMembership() {
        String[] stockDetails = {"MEMBERSHIP", "101"};
        assertEquals(new Video("MEMBERSHIP", 101), (itemParser.getItemFromString(stockDetails)));
    }

    @Test
    void shouldParseItemUpgradeMembership() {
        String[] stockDetails = {"UPGRADE_MEMBERSHIP", "101"};
        assertEquals(new Video("UPGRADE_MEMBERSHIP", 101), (itemParser.getItemFromString(stockDetails)));
    }
}