package com.thoughtworks.practice.parser.invoice;

import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import com.thoughtworks.practice.parser.StringParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InvoiceStringParserTest {
    private StringParser stringParser;

    @BeforeEach
    void setup() {
        stringParser = new InvoiceStringParser();
    }

    @Test
    void shouldGiveStringBookWhenGivenItemBook() {
        Item book = new Book("BOOK", 1, 2, 100);
        assertEquals("BOOK 1 100.0 2 200.0", stringParser.getStringFromItem(book));
    }

    @Test
    void shouldGiveStringVideoWhenGivenItemVideo() {
        Item video = new Video("VIDEO", 1, 50);
        assertEquals("VIDEO 1 50.0 1 50.0", stringParser.getStringFromItem(video));
    }

    @Test
    void shouldGiveParsedStringList() {
        List<Item> stockList = Arrays.asList(new Book("BOOK", 1, 2, 100), new Video("VIDEO", 1, 50));
        List<String> expected = Arrays.asList("BOOK 1 100.0 2 200.0", "VIDEO 1 50.0 1 50.0");
        assertEquals(expected, stringParser.parseStringList(stockList));
    }
}