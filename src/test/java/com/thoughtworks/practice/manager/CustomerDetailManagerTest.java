package com.thoughtworks.practice.manager;

import com.thoughtworks.practice.customer.Customer;
import com.thoughtworks.practice.customer.MemberCustomer;
import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class CustomerDetailManagerTest {

    @Test
    void shouldGiveTheDetailsOfACustomer() {
        CustomerDetailManager customerDetailManager = new CustomerDetailManager();
        MemberCustomer memberCustomer = new MemberCustomer();
        Customer customer = new Customer(101);
        customer.updateItemsBought(Arrays.asList(new Book("BOOK", 1, 2), new Video("VIDEO", 2)));
        memberCustomer.addMemberCustomer(101, customer);
        memberCustomer.addMemberCustomer(102, new Customer(102));
        List<Item> expected = Arrays.asList(new Book("BOOK", 1, 2), new Video("VIDEO", 2));
        assertEquals(expected, customerDetailManager.getCustomerDetails(101, memberCustomer));
    }

    @Test
    void shouldGiveNullWhenNoCustomerDetailsAreFound() {
        CustomerDetailManager customerDetailManager = new CustomerDetailManager();
        MemberCustomer memberCustomer = new MemberCustomer();
        memberCustomer.addMemberCustomer(101, new Customer(101));
        assertNull(customerDetailManager.getCustomerDetails(102, memberCustomer));
    }
}