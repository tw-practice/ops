package com.thoughtworks.practice.validator;

import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Membership;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CustomerValidatorTest {
    private CustomerValidator customerValidator;

    @BeforeEach
    void setup() {
        customerValidator = new CustomerValidator();
    }

    @Test
    void shouldBeFalseWhenMembershipIsNotIssuedToACustomer() {
        Item membership = new Membership("MEMBERSHIP", 101);
        assertFalse(customerValidator.isMembershipIssued(101, membership));
    }

    @Test
    void shouldBeTrueWhenMembershipIsIssuedToACustomer() {
        Item membership = new Membership("MEMBERSHIP", 101);
        membership.setQuantity(0);
        assertTrue(customerValidator.isMembershipIssued(101, membership));
    }
}