package com.thoughtworks.practice.customer;

import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Video;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MemberCustomerTest {
    private MemberCustomer memberCustomer;

    @BeforeEach
    void setup() {
        memberCustomer = new MemberCustomer();
    }

    @Test
    void shouldStoreTheMemberCustomers() {
        Customer customer = new Customer(101);
        customer.updateItemsBought(Arrays.asList(new Book("BOOK", 1, 2), new Video("VIDEO", 1)));
        memberCustomer.addMemberCustomer(101, customer);
        assertEquals(customer, memberCustomer.getMemberCustomers().get(101));
    }

    @Test
    void shouldGiveTheMemberCustomers() {
        Map<Integer, Customer> expected = new HashMap<>();
        expected.put(101, new Customer(101));
        expected.put(102, new Customer(102));
        memberCustomer.addMemberCustomer(101, new Customer(101));
        memberCustomer.addMemberCustomer(102, new Customer(102));
        assertEquals(expected, memberCustomer.getMemberCustomers());
    }
}