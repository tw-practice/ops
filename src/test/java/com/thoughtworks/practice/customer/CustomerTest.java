package com.thoughtworks.practice.customer;

import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CustomerTest {

    @Test
    void shouldGiveTheCustomerID() {
        Customer customer = new Customer(101);
        assertEquals(101, customer.getCustomerID());
    }

    @Test
    void shouldBeTrueWhenCustomerIsAValidMember() {
        Customer customer = new Customer(101);
        customer.setMember(true);
        assertTrue(customer.isMember());
    }

    @Test
    void shouldGiveTheListOfItemsBoughtByCustomer() {
        List<Item> itemsBought = Arrays.asList(new Book("BOOK", 1, 2), new Video("VIDEO", 2));
        Customer customer = new Customer(101);
        customer.updateItemsBought(itemsBought);
        assertEquals(itemsBought, customer.getItemsBought());
    }
}