package com.thoughtworks.practice;

import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.parser.invoice.InvoiceItemParser;

import java.util.ArrayList;
import java.util.List;

//Model the items ordered by a user
public class Cart {
    List<Item> itemsInCart;

    public Cart() {
        itemsInCart = new ArrayList<>();
    }

    public void addItems(List<String> selectedItems) {
        itemsInCart = new InvoiceItemParser().parseItemList(selectedItems);
    }

    public List<Item> getItemsInCart() {
        return itemsInCart;
    }
}