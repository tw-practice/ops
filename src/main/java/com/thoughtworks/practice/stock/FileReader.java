package com.thoughtworks.practice.stock;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//Read the Stock from a given file
public class FileReader {
    private List<String> fileContent;

    public FileReader() {
        fileContent = new ArrayList<>();
    }

    public List<String> getFileContent(String filePath) {
        readFromFile(filePath);
        return fileContent;
    }

    private void readFromFile(String filePath) {
        Scanner scannedFile = getScannedFile(filePath);
        readFileContent(scannedFile);
    }

    Scanner getScannedFile(String filePath) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return scanner;
    }

    private void readFileContent(Scanner scanner) {
        while (scanner.hasNextLine()) {
            String nextLine = scanner.nextLine();
            fileContent.add(nextLine);
        }
    }
}