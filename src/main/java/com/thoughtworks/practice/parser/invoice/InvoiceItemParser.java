package com.thoughtworks.practice.parser.invoice;

import com.thoughtworks.practice.item.*;
import com.thoughtworks.practice.parser.ItemParser;

//Parse the list of items associated with Invoice
public class InvoiceItemParser extends ItemParser {
    private static final int BOOK_COST = 100;
    private static final int VIDEO_COST = 50;
    private static final int MEMBERSHIP_COST = 200;
    private static final int UPGRADE_MEMBERSHIP_COST = 50;

    public Item getItemFromString(String[] inputDetails) {
        switch (inputDetails[0]) {
            case "BOOK":
                return new Book(inputDetails[0], Integer.parseInt(inputDetails[1]), Integer.parseInt(inputDetails[2]), BOOK_COST);
            case "VIDEO":
                return new Video(inputDetails[0], Integer.parseInt(inputDetails[1]), VIDEO_COST);
            case "MEMBERSHIP":
                return new Membership(inputDetails[0], Integer.parseInt(inputDetails[1]), MEMBERSHIP_COST);
        }
        return new UpgradeMembership(inputDetails[0], Integer.parseInt(inputDetails[1]), UPGRADE_MEMBERSHIP_COST);
    }
}