package com.thoughtworks.practice.parser;

import com.thoughtworks.practice.item.Item;

import java.util.ArrayList;
import java.util.List;

//Convert the list of strings to list of items
public abstract class ItemParser {

    public List<Item> parseItemList(List<String> stringList) {
        List<Item> items = new ArrayList<>();
        for (String string : stringList) {
            String[] stringItem = string.split(" ");
            items.add(getItemFromString(stringItem));
        }
        return items;
    }

    public abstract Item getItemFromString(String[] inputDetails);
}