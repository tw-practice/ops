package com.thoughtworks.practice.parser;

import com.thoughtworks.practice.item.Item;

import java.util.ArrayList;
import java.util.List;

//Convert the list of items to list of strings
public abstract class StringParser {

    public List<String> parseStringList(List<Item> itemList) {
        List<String> stringList = new ArrayList<>();
        for (Item item : itemList)
            stringList.add(getStringFromItem(item));
        return stringList;
    }

    public abstract String getStringFromItem(Item item);
}