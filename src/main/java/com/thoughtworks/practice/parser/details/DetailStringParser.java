package com.thoughtworks.practice.parser.details;

import com.thoughtworks.practice.item.*;
import com.thoughtworks.practice.parser.StringParser;

//Parse the list of string associated with Order Details
public class DetailStringParser extends StringParser {

    public String getStringFromItem(Item item) {
        if (item instanceof Book)
            return item.getName() + " " + item.getID() + " " + item.getQuantity();
        else if (item instanceof Video)
            return item.getName() + " " + item.getID();
        return item.getName() + " " + item.getID() + " " + ((Membership) item).getExpiry();
    }
}