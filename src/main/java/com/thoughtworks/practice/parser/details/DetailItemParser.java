package com.thoughtworks.practice.parser.details;

import com.thoughtworks.practice.item.*;
import com.thoughtworks.practice.parser.ItemParser;

//Parse the list of items associated with Order Details
public class DetailItemParser extends ItemParser {

    public Item getItemFromString(String[] inputDetails) {
        switch (inputDetails[0]) {
            case "BOOK":
                return new Book(inputDetails[0], Integer.parseInt(inputDetails[1]), Integer.parseInt(inputDetails[2]));
            case "VIDEO":
                return new Video(inputDetails[0], Integer.parseInt(inputDetails[1]));
            case "MEMBERSHIP":
                return new Membership(inputDetails[0], Integer.parseInt(inputDetails[1]));
        }
        return new UpgradeMembership(inputDetails[0], Integer.parseInt(inputDetails[1]));
    }
}