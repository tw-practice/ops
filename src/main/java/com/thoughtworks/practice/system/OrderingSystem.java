package com.thoughtworks.practice.system;

import com.thoughtworks.practice.Cart;
import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.UserInput;
import com.thoughtworks.practice.customer.MemberCustomer;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.manager.OrderManager;
import com.thoughtworks.practice.stock.Inventory;
import com.thoughtworks.practice.review.OrderReview;

import java.util.List;

//Model the order placed by the customer
public class OrderingSystem {
    private Cart cart = new Cart();
    private OrderManager orderManager = new OrderManager();


    public void placeOrder(Inventory inventory, MemberCustomer memberCustomers) {
        Order requestedOrder = getOrderFromUser();
        Order processedOrder = orderManager.getProcessedOrder(requestedOrder, inventory, memberCustomers);
        String reasonForFailure = orderManager.getReasonsForUnsuccessfulProcessing(requestedOrder, inventory);
        new OrderReview().analyse(processedOrder, reasonForFailure, inventory,memberCustomers);
    }

    private Order getOrderFromUser() {
        new UserInput().addSelectedItemsToCart(cart);
        List<Item> itemsInCart = cart.getItemsInCart();
        return new Order(itemsInCart);
    }
}