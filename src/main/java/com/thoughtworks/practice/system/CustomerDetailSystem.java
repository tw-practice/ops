package com.thoughtworks.practice.system;

import com.thoughtworks.practice.UserInput;
import com.thoughtworks.practice.customer.Customer;
import com.thoughtworks.practice.customer.MemberCustomer;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.manager.CustomerDetailManager;
import com.thoughtworks.practice.review.CustomerDetailReview;

import java.util.List;

//Model the details of items ordered by a customer
public class CustomerDetailSystem {

    public void customerDetail(MemberCustomer memberCustomers) {
        int customerID = getIDFromUser();
        List<Item> customerDetails = new CustomerDetailManager().getCustomerDetails(customerID, memberCustomers);
        Customer customer = new CustomerDetailManager().getCustomer(customerID,memberCustomers);
        new CustomerDetailReview().analyse(customerDetails, customer);
    }

    private int getIDFromUser() {
        return new UserInput().getIDForCustomerDetails();
    }
}
