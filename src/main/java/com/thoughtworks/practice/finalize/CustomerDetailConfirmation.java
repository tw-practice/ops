package com.thoughtworks.practice.finalize;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.customer.Customer;
import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import com.thoughtworks.practice.service.OrderService;
import com.thoughtworks.practice.ui.ConsoleIO;

import java.util.List;

//Finalize the customer details
public class CustomerDetailConfirmation {
    private ConsoleIO consoleIO = new ConsoleIO();

    public void confirm(List<Item> customerDetails, Customer customer) {
        displayCustomerDetails(customerDetails, customer);
    }

    private void displayCustomerDetails(List<Item> customerDetails, Customer customer) {
        consoleIO.display("The Customer Details are as follows:");
        String format = " %1$-18s %2$-8s";
        displayMembershipDetails(format, customerDetails, customer);
        displayBookDetails(format, customerDetails);
        displayVideoDetails(" %1$-18s", customerDetails);
    }

    private void displayVideoDetails(String format, List<Item> orderedItems) {
        consoleIO.display("Videos Taken:");
        consoleIO.display(String.format(format, "ID"));
        for (Item item : orderedItems) {
            if (item instanceof Video)
                consoleIO.display(String.format(format, item.getID()));
        }
    }

    private void displayBookDetails(String format, List<Item> orderedItems) {
        consoleIO.display("Books Taken:");
        consoleIO.display(String.format(format, "ID", "Number of Copies"));
        for (Item item : orderedItems) {
            if (item instanceof Book)
                consoleIO.display(String.format(format, item.getID(), item.getQuantity()));
        }
    }

    private void displayMembershipDetails(String format, List<Item> orderedItems, Customer customer) {
        consoleIO.display(String.format(format, "Member", customer.isMember() ? "Yes" : "No"));
        Order order = new Order(orderedItems);
        List<Item> members = new OrderService().getMembershipsPerOrder(order);
        int id = members.get(0).getID();
        consoleIO.display(String.format(format, "Membership ID", id));
    }
}