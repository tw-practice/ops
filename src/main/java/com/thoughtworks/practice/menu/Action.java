package com.thoughtworks.practice.menu;

import com.thoughtworks.practice.Application;
import com.thoughtworks.practice.customer.MemberCustomer;
import com.thoughtworks.practice.system.CustomerDetailSystem;
import com.thoughtworks.practice.system.OrderingSystem;
import com.thoughtworks.practice.stock.Inventory;
import com.thoughtworks.practice.ui.ConsoleIO;

import static java.lang.System.exit;

//Fulfill the user choice
public class Action {
    public void fulfill(int userChoice, Inventory inventory, MemberCustomer memberCustomers) {
        switch (userChoice) {
            case 1:
                new OrderingSystem().placeOrder(inventory, memberCustomers);
                break;
            case 2:
                new CustomerDetailSystem().customerDetail(memberCustomers);
                break;
            case 3:
                exit(0);
            default:
                new ConsoleIO().display("Invalid Choice");
                new Application().startProcessing(inventory, memberCustomers);
        }
    }
}