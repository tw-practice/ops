package com.thoughtworks.practice.ui;

//Model the input for Customer Details
public class CustomerDetailInputUI {
    private ConsoleIO consoleIO = new ConsoleIO();

    public int takeInput() {
        consoleIO.display("\nPlease enter the Customer ID, whose details you want to view:");
        String inputString = consoleIO.takeStringInput();
        String[] inputArray = inputString.split(" ");
        return Integer.parseInt(inputArray[1]);
    }
}