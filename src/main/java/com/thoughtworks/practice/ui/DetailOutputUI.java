package com.thoughtworks.practice.ui;

import java.util.List;

//Model the view of Order Details
public class DetailOutputUI {
    private ConsoleIO consoleIO = new ConsoleIO();

    public void showOrderDetails(List<String> orderDetails) {
        consoleIO.display("\nYour Order Details:");
        for (String detail : orderDetails) {
            String[] orderedItem = detail.split(" ");
            displayOrderedItem(orderedItem);
        }
    }

    private void displayOrderedItem(String[] orderedItem) {
        if (orderedItem[0].equals("BOOK"))
            consoleIO.display(String.format("%1$s, ID %2$s, Quantity %3$s", orderedItem[0], orderedItem[1], orderedItem[2]));
        else if (orderedItem[0].equals("VIDEO"))
            consoleIO.display(String.format("%1$s, ID %2$s", orderedItem[0], orderedItem[1]));
        else
            consoleIO.display(String.format("%1$s, ID %2$s, Expires on: %3$s", orderedItem[0], orderedItem[1], orderedItem[2]));
    }
}