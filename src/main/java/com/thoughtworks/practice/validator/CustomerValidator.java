package com.thoughtworks.practice.validator;

import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.service.InventoryService;
import com.thoughtworks.practice.stock.Inventory;

import java.util.List;

//Validates a customer as a system member
public class CustomerValidator {

    public boolean validateCustomer(int customerID, Inventory inventory) {
        List<Item> members = new InventoryService().getMembershipRequestsAvailableInStock(inventory);
        for (Item member : members)
            if (isMembershipIssued(customerID, member))
                return true;
        return false;
    }

    boolean isMembershipIssued(int customerID, Item member) {
        return member.getID() == customerID && member.getQuantity() == 0;
    }
}