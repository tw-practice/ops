package com.thoughtworks.practice.allocator;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.customer.Customer;
import com.thoughtworks.practice.customer.MemberCustomer;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.service.InventoryService;
import com.thoughtworks.practice.service.ItemOutOfStockException;
import com.thoughtworks.practice.service.OrderService;
import com.thoughtworks.practice.stock.Inventory;
import com.thoughtworks.practice.validator.CustomerValidator;

import java.util.List;

//Allocate the ordered items
public class StockAllocator {
    private InventoryService inventoryService = new InventoryService();

    public void allocate(Order requestedOrder, Inventory inventory) throws ItemOutOfStockException {
        allocateItemsFromInventory(requestedOrder, inventory);
    }

    public void allocate(Order requestedOrder, Inventory inventory, MemberCustomer memberCustomers) throws ItemOutOfStockException {
        allocateItemsFromInventory(requestedOrder, inventory);
        allocateItemsToCustomer(requestedOrder, inventory, memberCustomers);
    }

    private void allocateItemsToCustomer(Order requestedOrder, Inventory inventory, MemberCustomer memberCustomers) {
        Customer customer;
        List<Item> memberships = new OrderService().getMembershipsPerOrder(requestedOrder);
        if (memberships.size() == 1) {
            int customerID = memberships.get(0).getID();
            if (new CustomerValidator().validateCustomer(customerID, inventory)) {
                customer = getCustomer(requestedOrder, customerID);
                addCustomer(customerID, customer, memberCustomers);
            }
        }
    }

    private Customer getCustomer(Order requestedOrder, int customerID) {
        Customer customer;
        customer = new Customer(customerID);
        customer.setMember(true);
        customer.updateItemsBought(requestedOrder.getOrderedItems());
        return customer;
    }

    private void allocateItemsFromInventory(Order requestedOrder, Inventory inventory) throws
            ItemOutOfStockException {
        List<Item> orderedItems = requestedOrder.getOrderedItems();
        for (Item item : orderedItems)
            inventoryService.updateRemainingQuantity(item, inventory);
    }

    private void addCustomer(int customerID, Customer customer, MemberCustomer memberCustomers) {
        memberCustomers.addMemberCustomer(customerID, customer);
    }
}