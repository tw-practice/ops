package com.thoughtworks.practice;

//Entry point of the Order Processing Application
public class Main {

    public static void main(String[] args) {
        Application application = new Application();
        application.start();
    }
}