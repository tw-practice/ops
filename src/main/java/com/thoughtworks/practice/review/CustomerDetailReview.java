package com.thoughtworks.practice.review;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.OrderConfirmation;
import com.thoughtworks.practice.customer.Customer;
import com.thoughtworks.practice.finalize.CustomerDetailConfirmation;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.stock.Inventory;
import com.thoughtworks.practice.ui.ConsoleIO;
import com.thoughtworks.practice.ui.ReasonUI;

import java.util.List;

//Analyse an order for finalization
public class CustomerDetailReview {

    public void analyse(List<Item> customerDetails, Customer customer) {
        if (isCustomerDetailProcessedSuccessfully(customerDetails))
            new CustomerDetailConfirmation().confirm(customerDetails, customer);
        else
            new ConsoleIO().display(getReasonForFailure());
    }

    private String getReasonForFailure() {
        return "\n We do not have your details as your are not our member customer.";
    }

    boolean isCustomerDetailProcessedSuccessfully(List<Item> customerDetails) {
        return customerDetails != null;
    }
}