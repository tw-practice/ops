package com.thoughtworks.practice.review;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.OrderConfirmation;
import com.thoughtworks.practice.customer.MemberCustomer;
import com.thoughtworks.practice.stock.Inventory;
import com.thoughtworks.practice.ui.ReasonUI;

//Analyse an order for finalization
public class OrderReview {

    public void analyse(Order processedOrder, String reason, Inventory inventory, MemberCustomer memberCustomer) {
        if (isOrderProcessedSuccessfully(processedOrder))
            new OrderConfirmation().confirm(processedOrder, inventory, memberCustomer);
        else
            new ReasonUI().showReasonForFailure(reason);
    }

    boolean isOrderProcessedSuccessfully(Order processedOrder) {
        return processedOrder != null;
    }
}