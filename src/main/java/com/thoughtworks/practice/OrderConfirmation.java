package com.thoughtworks.practice;

import com.thoughtworks.practice.customer.MemberCustomer;
import com.thoughtworks.practice.generator.DetailGenerator;
import com.thoughtworks.practice.generator.InvoiceGenerator;
import com.thoughtworks.practice.stock.Inventory;
import com.thoughtworks.practice.ui.ConsoleIO;

//Finalize the Accepted Order
public class OrderConfirmation {

    public void confirm(Order processedOrder, Inventory inventory, MemberCustomer memberCustomer) {
        displayOrderSummary(processedOrder);
        if (isUserRequestingInvoice())
            displayInvoice(processedOrder);
        new Application().startProcessing(inventory,memberCustomer);
    }

    private boolean isUserRequestingInvoice() {
        return new ConsoleIO().isInvoiceRequested();
    }

    private void displayInvoice(Order processedOrder) {
        new InvoiceGenerator().generateInvoice(processedOrder);
    }

    private void displayOrderSummary(Order processedOrder) {
        new DetailGenerator().generateOrderDetails(processedOrder);
    }
}