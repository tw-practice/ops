package com.thoughtworks.practice.generator;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.parser.details.DetailStringParser;
import com.thoughtworks.practice.ui.DetailOutputUI;

import java.util.List;

//Generates the order details
public class DetailGenerator {

    public void generateOrderDetails(Order processedOrder) {
        List<Item> processedItems = processedOrder.getOrderedItems();
        List<String> parsedString = new DetailStringParser().parseStringList(processedItems);
        new DetailOutputUI().showOrderDetails(parsedString);
    }
}