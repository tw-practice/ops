package com.thoughtworks.practice.customer;

import com.thoughtworks.practice.item.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

//Maintain the items ordered by a member customer
public class Customer {
    private int customerID;
    private boolean isMember;
    private List<Item> itemsBought = new ArrayList<>();

    public Customer(int CustomerID) {
        this.customerID = CustomerID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public boolean isMember() {
        return isMember;
    }

    public List<Item> getItemsBought() {
        return itemsBought;
    }

    public void setMember(boolean member) {
        isMember = member;
    }

    public void updateItemsBought(List<Item> items) {
        for (Item item : items)
            if (itemsBought.contains(item)) {
                int itemIndex = itemsBought.indexOf(item);
                int itemQuantity = itemsBought.get(itemIndex).getQuantity();
                itemQuantity += item.getQuantity();
                itemsBought.get(itemIndex).setQuantity(itemQuantity);
            } else
                itemsBought.add(item);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer)) return false;
        Customer customer = (Customer) o;
        return customerID == customer.customerID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerID);
    }
}