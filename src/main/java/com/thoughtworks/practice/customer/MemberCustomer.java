package com.thoughtworks.practice.customer;

import java.util.HashMap;
import java.util.Map;

//Store the details of all member customers of the system
public class MemberCustomer {
    private Map<Integer, Customer> memberCustomers;

    public MemberCustomer() {
        this.memberCustomers = new HashMap<>();
    }

    public Map<Integer, Customer> getMemberCustomers() {
        return memberCustomers;
    }

    public void addMemberCustomer(int customerID, Customer customer) {
        if (memberCustomers.containsKey(customerID))
            memberCustomers.get(customerID).updateItemsBought(customer.getItemsBought());
        else
            memberCustomers.put(customerID, customer);
    }
}