package com.thoughtworks.practice;

import com.thoughtworks.practice.ui.CustomerDetailInputUI;
import com.thoughtworks.practice.ui.DetailInputUI;

import java.util.List;

//Fetch the user input and add it to the cart
public class UserInput {

    public void addSelectedItemsToCart(Cart cart) {
        List<String> selectedItems = new DetailInputUI().takeInput();
        cart.addItems(selectedItems);
    }

    public int getIDForCustomerDetails() {
        return new CustomerDetailInputUI().takeInput();
    }
}