package com.thoughtworks.practice.item;

import java.time.LocalDate;

//Model the customer request to upgrade the membership of a customer
public class UpgradeMembership extends Membership {

    public UpgradeMembership(String name, int ID, double cost) {
        super(name, ID, cost);
    }

    public UpgradeMembership(String name, int ID) {
        super(name, ID);
    }

    @Override
    public LocalDate getExpiry() {
        setExpiry();
        return super.getExpiry();
    }

    private void setExpiry() {
        expiry = expiry.plusMonths(1);
    }
}