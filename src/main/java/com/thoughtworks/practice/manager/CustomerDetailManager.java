package com.thoughtworks.practice.manager;

import com.thoughtworks.practice.customer.Customer;
import com.thoughtworks.practice.customer.MemberCustomer;
import com.thoughtworks.practice.item.Item;

import java.util.List;

//Fetch the Customer Details
public class CustomerDetailManager {

    public List<Item> getCustomerDetails(int customerID, MemberCustomer memberCustomers) {
        if (memberCustomers.getMemberCustomers().containsKey(customerID))
            return getCustomer(customerID, memberCustomers).getItemsBought();
        return null;
    }

    public Customer getCustomer(int customerID, MemberCustomer memberCustomers) {
        if (memberCustomers.getMemberCustomers().containsKey(customerID))
            return memberCustomers.getMemberCustomers().get(customerID);
        return null;
    }
}